import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterializeModule } from 'angular2-materialize';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { OurStoryComponent } from './our-story/our-story.component';
import { WhenWhereComponent } from './when-where/when-where.component';
import { FromFileComponentComponent } from './from-file-component/from-file-component.component';
import { FromMicrophoneComponent } from './from-microphone/from-microphone.component';
import { TheoricComponentComponent } from './theoric-component/theoric-component.component';

@NgModule({
  declarations: [
    AppComponent,
    OurStoryComponent,
    WhenWhereComponent,
    FromFileComponentComponent,
    FromMicrophoneComponent,
    TheoricComponentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'theoric-component', pathMatch: 'full' },
      { path: 'theoric-component', component: TheoricComponentComponent },
      { path: 'from-file-component', component: FromFileComponentComponent },
      { path: 'from-microphone', component: FromMicrophoneComponent }
    ]),
    MaterializeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
