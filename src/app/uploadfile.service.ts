import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType, HttpRequest, HttpErrorResponse, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class UploadfileService {
  apiBaseURL = 'http://127.0.0.1:5001/api/';

  constructor(private http: HttpClient) { }

  fileUpload(fileItem: File): any {
    let apiCreateEndpoint = `${this.apiBaseURL}files/create/`;
    const formData: FormData = new FormData();
    formData.append('fileItem', fileItem, fileItem.name);
    const req = new HttpRequest('POST', apiCreateEndpoint, formData, {
      reportProgress: true // for progress data
    });
    return this.http.request(req);
  }

}
