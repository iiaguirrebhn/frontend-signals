import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http'

@Component({
  selector: 'app-from-microphone',
  templateUrl: './from-microphone.component.html',
  styleUrls: ['./from-microphone.component.css'],
})
export class FromMicrophoneComponent implements OnInit {
  showForm: boolean = true;
  showrRecording: boolean = false;
  ShowRequestResult: boolean = false;
  urlToFig: String ='http://localhost:5002/'

  constructor(private http: HttpClient) {

  }

  ngOnInit() {
  }

  recordSound() {
    this.showrRecording = true;
    this.ShowRequestResult = false;
    this.http.get<any>(`http://localhost:5002/fromMicro`).subscribe(res => {
      const resObj = res;
      if (res.message == 'succes') {
        this.showrRecording = false;
        this.ShowRequestResult = true;
        this.showrRecording=false;
        this.urlToFig = this.urlToFig + res.url;
      }
    });

  }

}
