import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http'

@Component({
  selector: 'app-from-file-component',
  templateUrl: './from-file-component.component.html',
  styleUrls: ['./from-file-component.component.css'],
})
export class FromFileComponentComponent implements OnInit {
  showForm: boolean = true;
  progress: number;
  showPreoloader: boolean = false;
  ShowRequestResult: boolean = false;
  urlToFig: String ='http://localhost:5002/'

  constructor(private http: HttpClient) {

  }

  ngOnInit() {

  }
  upload(input) {
    this.showPreoloader = true;
    this.ShowRequestResult = false;
    const formData = new FormData();
    input = document.querySelector('input[type="file"]')
    formData.append('file', input.files[0])
    this.http.post<any>(`http://localhost:5002/fromFile`, formData).subscribe( res => {
      const resObj = res;
      if(res.message =='succes' ){
        this.showPreoloader = false;
        this.ShowRequestResult = true;
        this.urlToFig = this.urlToFig + res.url;
      }
    });

  }
}
